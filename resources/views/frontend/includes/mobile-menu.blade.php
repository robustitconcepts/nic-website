<nav class="navbar navbar-inverse navbar-red visible-xs">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Menu</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="#">Who We Are</a></li>
        <li><a href="#">What We Do</a></li>
        <li><a href="#">Procedure for Request</a></li>
        <li><a href="#">Rights to Know</a></li>
        

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Commision <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Working Procedure</a></li>
            <li><a href="#">Press Release</a></li>
            <li><a href="#">Diary</a></li>
            <li><a href="#">Important News</a></li>
            <li><a href="#">Right Duties and Responsibilites</a></li>
          </ul>

        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Publications <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Working Procedure</a></li>
            <li><a href="#">Press Release</a></li>
            <li><a href="#">Diary</a></li>
            <li><a href="#">Important News</a></li>
            <li><a href="#">Right Duties and Responsibilites</a></li>
          </ul>

        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Decisions <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Working Procedure</a></li>
            <li><a href="#">Press Release</a></li>
            <li><a href="#">Diary</a></li>
            <li><a href="#">Important News</a></li>
            <li><a href="#">Right Duties and Responsibilites</a></li>http://127.0.0.1/OneDrive/NIC/template/#
          </ul>

        </li>

        <li><a href="#">Related Laws</a></li>
        <li><a href="#">Information Officers</a></li>
        <li><a href="#">Applicats/Application Sample</a></li>
        <li><a href="#">Individual Publications</a></li>
        <li><a href="#">Vatieties</a></li>

        <li><a href="#">Login - RTI MIS Online</a></li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Language <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Nepali</a></li>
            <li><a href="#">English</a></li>
          </ul>

        </li>

      </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>